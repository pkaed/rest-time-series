======
Series
======

Series is a simplae Django app to maintain time series data points, and present them in a REST API.

Quick start
-----------

1. Add "series" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'series',
    )

2. Include the polls URLconf in your project urls.py like this::

    from series.api import SerieResource
    series_resource = SerieResource()

    urlpatterns = patterns('',
        ...
        (r'^api/', include(series_resource.urls)),
    )

3. Run `python manage.py migrate` to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create series & data points (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/rest/1.0/time-series/?format=json to explore.
