from django.db import models

class Serie(models.Model):
  name = models.CharField(max_length=30)
  source_name = models.CharField(max_length=30)
  source_url = models.CharField(max_length=150)
  pub_date = models.DateTimeField('date published')

  def __unicode__(self):
    return self.name

class DataPoint(models.Model):
  serie = models.ForeignKey(Serie, related_name='data')
  date = models.DateTimeField()
  value = models.FloatField()

  def __unicode__(self):
    return '%s %f' % (self.date, self.value)
