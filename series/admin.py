from django.contrib import admin
from series.models import Serie, DataPoint

class DataPointInline(admin.TabularInline):
  model = DataPoint

class SerieAdmin(admin.ModelAdmin):
  inlines = [DataPointInline]

# Register your models here.
admin.site.register(Serie, SerieAdmin)
admin.site.register(DataPoint)
