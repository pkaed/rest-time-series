from django.conf.urls import patterns, include, url
from series.api import SerieResource, DataPointResource

series_resource = SerieResource()
data_point_resource = DataPointResource()

urlpatterns = patterns('',
    url(r'', include(series_resource.urls)),
#    url(r'', include(data_point_resource.urls)),
)

