from django.conf.urls import url
from django.core.urlresolvers import reverse
from django.db.models import Q
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from tastypie import fields
from series.models import Serie, DataPoint


class SerieResource(ModelResource):
    data = fields.CharField(readonly=True)
    
    def dehydrate_data(self, bundle):
        return reverse("api_get_children", kwargs={'resource_name': self._meta.resource_name, 'pk': bundle.obj.id})

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/data%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_children'), name="api_get_children"),
        ]

    def get_children(self, request, **kwargs):
        try:
            bundle = self.build_bundle(data={'id': kwargs['pk']}, request=request)
            obj = self.cached_obj_get(bundle=bundle, **self.remove_api_resource_names(kwargs))
        except ObjectDoesNotExist:
            return HttpGone()
        except MultipleObjectsReturned:
            return HttpMultipleChoices("More than one resource is found at this URI.")

        child_resource = DataPointResource()
        child_resource.serie = obj
        return child_resource.get_list(request, serie=obj)

    class Meta:
        queryset = Serie.objects.all()
        resource_name = 'series'

class DataPointResource(ModelResource):
   def apply_filters(self, request, applicable_filters):
       semi_filtered = super(DataPointResource, self).apply_filters(request, applicable_filters)
       return semi_filtered.filter(Q(serie = self.serie)) if self.serie else semi_filtered
   
   class Meta:
        queryset = DataPoint.objects.all().order_by('-date')
        excludes = ['id', 'resource_uri']
        resource_name = 'data'
        include_resource_uri = False
        filtering = {
            "date": ("lte", "lt", "gte", "gt",),
        }

